#/bin/sh
set -e

FILE=${1}
IMAGE=${CI_REGISTRY_IMAGE}:${FILE}

docker build -t ${IMAGE} -f ./${FILE} .
docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
docker push ${IMAGE}