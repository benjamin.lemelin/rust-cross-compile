# Rust Cross Compile

Docker Images for Rust cross compilation. Especially useful in a CI environment like GitLab.

## Usage

Pull the image from the GitLab container registry. Use the tag to choose your target. In a `.gitlab-ci.yml` file, 
this unusually looks like this :

```yml
stages:
  - build

build:
  stage: build
  image: registry.gitlab.com/benjamin.lemelin/rust-cross-compile:x86_64-unknown-linux-musl
  script:
    - cargo build --target x86_64-unknown-linux-musl --release
  artifacts:
    paths:
      - target/x86_64-unknown-linux-musl/release/your-app
    expire_in: 1 week
```

## License

This project is licensed under the MIT License. See the [LICENSE.md](LICENSE.md) file for details.